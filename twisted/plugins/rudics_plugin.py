#!/usr/bin/env python
#
# $Id: rudics_plugin.py,v 4cbbd9cbf445 2008/06/10 23:43:08 mikek $
#
#
from zope.interface import implements
from twisted.application import internet, service
from twisted.application.service import IServiceMaker
from twisted.internet.protocol import Factory
from twisted.cred.portal import Portal
from twisted.plugin import IPlugin
from twisted.python import usage, log
from twisted.web.client import getPage
from ConfigParser import ConfigParser
from rudics import protocol, auth
import os
try:
    import json
except ImportError:
    import simplejson as json


class Options(usage.Options):
    synopsis = "[options]"
    longdesc = "Make a RUDICS server."
    optParameters = [
        ['port', 'p', 10130, 'TCP port to listen on'],
        ['timeout', 't', 60, 'session timeout in seconds'],
        ['config', 'c', os.path.expanduser('~/rudics.cfg'),
         'configuration file'],
        ['server', 's', 'http://localhost:5984', 'CouchDB server url'],
        ['dbname', 'd', 'rudics', 'CouchDB database name'],
        ['auth', 'a', None, 'database credentials file, username:password']
    ]

    def postOptions(self):
        cfg = ConfigParser()
        cfg.read(self['config'])
        for key in ('allow', 'deny'):
            if cfg.has_option('access', key):
                self[key] = cfg.get('access', key)
            else:
                self[key] = '0.0.0.0/0'


class RudicsFactory(Factory):
    def __init__(self, portal, func):
        self.portal = portal
        self.func = func

    def buildProtocol(self, addr):
        p = protocol.Gatekeeper(self.portal,
                                record_session=self.func, login_timeout=10)
        p.factory = self
        return p


class RudicsServiceMaker(object):
    implements(IServiceMaker, IPlugin)

    tapname = "rudics"
    description = "RUDICS server."
    options = Options
    cache = []

    def _add_to_cache(self, doc_id, doc, err=None):
        doc['_id'] = doc_id
        self.cache.append(doc)
        if err:
            log.err(err)

    def _clear_cache(self):
        self.cache = []

    def _dump_cache(self):
        """
        Return the cache as a JSON string suitable for the CouchDB
        Bulk Upload API.
        """
        return json.dumps({'docs': self.cache})

    def _saveSession(self, irsn, stats):
        """
        Upload session statistics to the CouchDB server. If the server
        cannot be reached, the data are cached for upload at a later
        time.
        """
        if irsn:
            doc = {'type': 'session',
                   'bytes_in': stats['transfer'][0],
                   'bytes_out': stats['transfer'][1],
                   'imei': str(irsn)}
            for ev, t in stats['events']:
                doc[ev] = int(t)
            session = json.dumps(doc)
            headers = {'content-type': 'text/plain'}
            doc_id = '%s_%08x' % (doc['imei'], doc['connect'])
            if self.auth:
                headers['authorization'] = 'Basic ' + protocol.encode_auth(*self.auth)
            if len(self.cache) > 0:
                # Add this document to the cache and try to upload using
                # the Bulk Documents API
                self._add_to_cache(doc_id, doc)
                url = '/'.join([self.server, self.dbname, '_bulk_docs'])
                d = getPage(url, method='POST',
                            postdata=self._dump_cache(), headers=headers)
                d.addCallback(lambda result: log.msg(repr(result)))
                d.addCallback(lambda _: self._clear_cache())
                d.addErrback(log.err)
            else:
                # Try the standard document upload. Add the document to
                # the cache on failure
                url = '/'.join([self.server, self.dbname, doc_id])
                d = getPage(url, method='PUT',
                            postdata=session, headers=headers)
                d.addCallback(lambda result: log.msg(repr(result)))
                d.addCallback(lambda _: log.msg(repr(doc)))
                d.addErrback(log.err)

    def makeService(self, config):
        self.server = config['server']
        self.dbname = config['dbname']
        if config['auth']:
            self.auth = open(config['auth'], 'r').read().strip().split(':')
        else:
            self.auth = None
        allow_hosts = config['allow'].split()
        deny_hosts = config['deny'].split()
        portal = Portal(auth.IrsnRealm(timeout=int(config['timeout'])))
        checker = auth.IrsnCouchdb(self.server, self.dbname, username='mlf2')
        portal.registerChecker(checker)
        f = protocol.AllowDenyPeer(RudicsFactory(portal, self._saveSession),
                                   allow_from=allow_hosts,
                                   deny_from=deny_hosts)
        svc = service.MultiService()
        internet.TCPServer(int(config['port']), f).setServiceParent(svc)
        return svc

serviceMaker = RudicsServiceMaker()
