#!/usr/bin/env python
#
# $Id: test_redirect.py,v cb923b00aaa2 2008/06/08 06:19:42 mikek $
#
from twisted.trial import unittest, util
from twisted.internet import reactor, defer
from twisted.internet.protocol import Protocol, ClientFactory, ClientCreator, Factory
from twisted.protocols.basic import LineReceiver
from twisted.protocols.policies import TimeoutMixin
from twisted.cred.portal import Portal
from twisted.python import log
from rudics import protocol, auth
from rudics.store import Device, Session
from axiom.store import Store
from epsilon.extime import Time
import pwd
import os

class TestFactory(Factory):
    def __init__(self, portal, func):
        self.portal = portal
        self.func = func
        
    def buildProtocol(self, addr):
        p = protocol.Gatekeeper(self.portal,
                                record_session=self.func, login_timeout=5)
        p.factory = self
        return p

class TestProtocol(LineReceiver):
    delimiter = '\r\n'
    def connectionMade(self):
        self.transport.write('>')

    def lineReceived(self, line):
        self.factory.d.callback(line)
        self.transport.loseConnection()

class TestClient(LineReceiver):
    delimiter = 'irsn: '
    state = 'start'
    def lineReceived(self, line):
        getattr(self, 'state_%s' % self.state.upper())(line)

    def state_START(self, line):
        self.transport.write(self.factory.irsn + '\r\n')
        self.state = 'auth'
        self.delimiter = '>'
        
    def state_AUTH(self, line):
        self.factory.d.callback(self)
        self.state = 'done'

    def state_DONE(self, line):
        pass
    
    def sendMessage(self, msg):
        self.transport.write(msg + '\r\n')

class ShellClient(TestClient):
    def state_START(self, line):
        self.transport.write(self.factory.irsn + '\r\n')
        self.state = 'auth'
        self.delimiter = '$'
        
    def state_AUTH(self, line):
        self.factory.d.callback(True)
        self.state = 'done'
        self.transport.write('exit\r\n')
    
class BadClient(LineReceiver, TimeoutMixin):
    delimiter = 'irsn: '

    def lineReceived(self, line):
        self.transport.write('qwerty\r\n')

    def connectionMade(self):
        self.setTimeout(10)
        
    def connectionLost(self, reason):
        self.setTimeout(None)
        self.factory.d.callback(True)

    def timeoutConnection(self):
        self.factory.d.callback(False)

class TimeoutClient(LineReceiver, TimeoutMixin):
    def connectionMade(self):
        self.setTimeout(30)

    def connectionLost(self, reason):
        self.setTimeout(None)
        self.factory.d.callback(True)

    def timeoutConnection(self):
        self.factory.d.callback(False)

class RedirectTestCase(unittest.TestCase):
    def shortDescription (self):
        return "Test RUDICS server redirection"

    def saveSession(self, irsn, stats):
        if irsn:
            device = self.store.findOrCreate(Device, irsn=unicode(irsn))
            kwds = {'bytes_in' : stats['transfer'][0],
                    'bytes_out' : stats['transfer'][1]}
            for ev, t in stats['events']:
                kwds[ev] = Time.fromPOSIXTimestamp(t)
            session = device.newSession(**kwds)
            log.msg(repr(session))
    
    def setUp(self):
        self.irsn = 'x0123456789'
        self.port = 5678
        self.clients = []
        self.servers = []
        self.store = Store()
        dev = Device(store=self.store, irsn=unicode(self.irsn),
                     forward=unicode('%s:%d' % ('localhost', self.port)))
        user = pwd.getpwuid(os.getuid())
        portal = Portal(auth.IrsnRealm())
        checker = auth.IrsnDatabase(self.store, username=user.pw_name)
        portal.registerChecker(checker)
        f = TestFactory(portal, self.saveSession)
        self.d = defer.Deferred()
        self.listen_port = reactor.listenTCP(0, f, interface="127.0.0.1")
        self.servers.append(self.listen_port)
        
    def testRedirect(self):
        """Test login and redirection to a separate server"""
        msg = 'asdfghjkl'
        c = ClientFactory()
        c.protocol = TestClient
        c.d = defer.Deferred()
        c.irsn = self.irsn
        c.d.addCallback(lambda p: p.sendMessage(msg))
        svr = Factory()
        svr.protocol = TestProtocol
        svr.d = self.d
        self.d.addCallback(lambda r: self.failUnless(r == msg,
                                                     'unexpected result, %s' % repr(r)))
        self.servers.append(reactor.listenTCP(self.port, svr, interface='127.0.0.1'))
        self.clients.append(reactor.connectTCP('127.0.0.1',
                                               self.listen_port.getHost().port, c))
        return self.d

    def testShell(self):
        """Test login to local shell"""
        c = ClientFactory()
        c.protocol = ShellClient
        c.d = self.d
        c.irsn = '0123456789'
        self.d.addCallback(lambda r: self.failUnless(r))
        self.clients.append(reactor.connectTCP('127.0.0.1',
                                               self.listen_port.getHost().port, c))
        return self.d

    def testBadirsn(self):
        """Test detection of a bad IRSN"""
        c = ClientFactory()
        c.protocol = BadClient
        c.d = self.d
        self.d.addCallback(lambda r: self.failUnless(r))
        self.clients.append(reactor.connectTCP('127.0.0.1',
                                               self.listen_port.getHost().port, c))
        return self.d

    def testTimeout(self):
        """Test authentication timeout"""
        c = ClientFactory()
        c.protocol = TimeoutClient
        c.d = self.d
        self.d.addCallback(lambda r: self.failUnless(r))
        self.clients.append(reactor.connectTCP('127.0.0.1',
                                               self.listen_port.getHost().port, c))
        return self.d

    def tearDown(self):
        dl = defer.DeferredList([s.stopListening() for s in self.servers])
        dl.addCallback(lambda _: [c.disconnect() for c in self.clients])
        return dl
