
Twisted-based RUDICS Server
===========================

Introduction
------------

RUDICS_ is a service provided by Iridium_ to allow data connections from a
mobile device to a TCP server. This server provides an endpoint for that
service.

.. _RUDICS: http://www.nalresearch.com/NetRef_RUDICS.html
.. _Iridium: http://www.iridium.com/

The server differentiates connections using the mobile device's Iridium Serial
Number (IRSN). Connections from MLF2 floats are provided with a local shell
log-in while others a provided a transparent connenction to a separate TCP
server (referred to in this document as the downstream server)


Installation
------------

Pre-requisites:  Twisted >= 2.5

::

  $ sudo python setup.py install
  $ sudo rudics-install /usr/local/bin

The second command installs the script ``rudics.sh`` in ``/usr/local/bin``. This
script should be used to start and stop the server.

To use the host-based access control feature, create the file
``/usr/local/etc/rudics.cfg`` and add the following content::

  [access]
  allow = <List of IP addresses>
  deny = <List of IP addresses>

The address list is a space-separated list of single addresses or
network-block specifications of the form: N.N.N.N/M Where 'M' is the number of
bits in the network part of the address, e.g.::

  [access]
  allow = 12.47.179.0/24 127.0.0.1
  deny = 0.0.0.0/0

The example above allows connections from Iridium's network block and from the
loopback interface.  Connections are denied from anywhere else.

Database
--------

The server uses a CouchDB_ database to store the mapping between IRSN and the
downstream server along with session logging information. By default, CouchDB
is expected to be running on the local system (localhost) but this can be
changed by editting the variables near the top of the
``/usr/local/bin/rudics.sh`` script.

New entries can be added to the database using the
``rudics-update`` program
::
 
  $ rudics-update infile

``infile`` is a CSV file where each line has the following fields::

  email,IRSN,IPaddress:port,device name

The database can be dumped in CSV format with the ``rudics-dump`` program::

  $ rudics-dump > rudics.csv


.. _CouchDB: http://couchbase.org/


Running the server
------------------

::
  $ sudo /usr/local/bin/rudics.sh start
  $ sudo /usr/local/bin/rudics.sh stop

Place the first line (minus the ``sudo``) in ``/etc/rc.d/rc.local`` if you
want the server to start automatically at boot time.
