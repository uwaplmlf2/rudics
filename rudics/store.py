#!/usr/bin/env python
#
# $Id: store.py,v 8c9da56e8ab6 2008/06/10 18:19:57 mikek $
#
from axiom import item, attributes
from zope.interface import Interface, Attribute, implements


class IMLF2Extras(Interface):
    """Extra attributes for MLF2 devices."""
    floatid = Attribute('MLF2 float ID number')
    url = Attribute('MLF2 data url for this float')


class Owner(item.Item):
    typeName = 'Owner'
    schemaVersion = 1

    email = attributes.text(allowNone=False)
    notify = attributes.boolean(default=False)

    def newDevice(self, **kwds):
        return Device(store=self.store, owner=self, **kwds)


class Device(item.Item):
    typeName = 'Device'
    schemaVersion = 1

    owner = attributes.reference()
    irsn = attributes.text(doc='Iridium serial number', allowNone=False)
    name = attributes.text()
    forward = attributes.text(doc='host:port to forward this connection to')

    def newSession(self, **kwds):
        return Session(store=self.store, device=self, **kwds)

    def sessions(self):
        return self.store.query(Session,
                                Session.device == self,
                                sort=Session.connect.descending)


class MLF2Device(item.Item):
    implements(IMLF2Extras)

    typeName = 'MLF2Device'
    schemeVersion = 1

    floatid = attributes.integer(allowNone=False)
    url = attributes.text(default=u'http://mlf2srvr.apl.washington.edu/')

    def installOn(self, device):
        for powerup in device.powerupsFor(IMLF2Extras):
            device.powerDown(powerup, IMLF2Extras)
        device.powerUp(self, IMLF2Extras)


class Session(item.Item):
    typeName = 'Session'
    schemaVersion = 1

    device = attributes.reference()
    connect = attributes.timestamp(allowNone=False)
    login = attributes.timestamp()
    logout = attributes.timestamp()
    bytes_in = attributes.integer(default=0)
    bytes_out = attributes.integer(default=0)

    def __repr__(self):
        tmpl = '<Session irsn={} bytes={:d}/{:d} login={} logout={}>'
        return tmpl.format(self.device.irsn,
                           self.bytes_in,
                           self.bytes_out,
                           str(self.login),
                           str(self.logout))

    def mlf2compat(self):
        """
        Return this item as a list of key,value pairs with keys compatible with
        the MLF2 Iridium session records.
        """
        return [('in', self.bytes_in),
                ('out', self.bytes_out),
                ('connect', int(self.connect.asPOSIXTimestamp())),
                ('login', int(self.login.asPOSIXTimestamp())),
                ('logout', int(self.logout.asPOSIXTimestamp()))]
