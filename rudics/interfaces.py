#
# $Id: interfaces.py,v fda09431bfcc 2008/05/18 06:37:19 mikek $
#
# Interfaces and adapters.
#
from twisted.internet.interfaces import ITransport
from zope.interface import implements
from twisted.internet.protocol import Protocol
from twisted.python import components


class TransportFromProtocol:
    implements(ITransport)

    def __init__(self, original):
        self.original = original

    def __getattr__(self, name):
        return getattr(self.original.transport, name)


components.registerAdapter(
    TransportFromProtocol,
    Protocol,
    ITransport)
