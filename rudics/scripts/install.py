#!/usr/bin/env python
#
# $Id: install.py,v e8c3ce3731b7 2008/06/10 22:48:44 mikek $
#
from pkg_resources import resource_string
import os
from optparse import OptionParser


def _install_resource(rsrc, dest, mode=0755):
    if os.path.isdir(dest):
        filename = os.path.join(dest, rsrc)
    else:
        filename = dest
    print 'Installing %s' % filename
    ofile = open(filename, 'w')
    ofile.write(resource_string(__name__, rsrc))
    ofile.close()
    os.chmod(filename, mode)


def install_startup():
    usage = """usage: %prog directory

    Install the service start/stop script in the specified
    directory.
    """
    parser = OptionParser(usage=usage)
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('No directory specified')

    _install_resource('rudics.sh', args[0])


if __name__ == '__main__':
    install_startup()
