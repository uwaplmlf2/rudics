#!/usr/bin/env python
#
# Add new entries to the RUDICS database.
#
from optparse import OptionParser
from couchdb import client
import sys


def main():
    """
    %prog [options] imei [imei ...]

    Delete the listed IMEI entries from the RUDICS database
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(server="http://localhost:5984", dbname="rudics")
    parser.add_option("-s", "--server",
                      type="string",
                      dest="server",
                      metavar="URL",
                      help="URL of CouchDB server (%default)")
    parser.add_option("-d", "--database",
                      type="string",
                      dest="dbname",
                      metavar="NAME",
                      help="name of CouchDB database (%default)")
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('No IMEI specified')

    server = client.Server(opts.server)
    db = server[opts.dbname]

    for imei in args:
        doc = db.get(imei)
        if doc:
            db.delete(doc)
            sys.stderr.write('Deleting %s\n' % imei)
        else:
            sys.stderr.write('%s entry not found\n' % imei)

if __name__ == '__main__':
    main()
