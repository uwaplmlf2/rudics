#!/usr/bin/env python
#
# Dump the RUDICS database in CSV format
#
from optparse import OptionParser
from couchdb import client


def main():
    """
    %prog [options]

    Dump the RUDICS database to standard output in CSV format
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(server="http://localhost:5984", dbname="rudics")
    parser.add_option("-s", "--server",
                      type="string",
                      dest="server",
                      metavar="URL",
                      help="URL of CouchDB server (%default)")
    parser.add_option("-d", "--database",
                      type="string",
                      dest="dbname",
                      metavar="NAME",
                      help="name of CouchDB database (%default)")
    opts, args = parser.parse_args()

    server = client.Server(opts.server)
    db = server[opts.dbname]

    for row in db.view('rudics/devices'):
        doc = row.value
        if doc['type'] == 'local':
            print ','.join(['mlf2', doc['imei'], '', doc['name']])
        elif doc['type'] == 'proxy':
            print ','.join([doc['owner'],
                            doc['imei'],
                            doc['endpoint'],
                            doc['name']])

if __name__ == '__main__':
    main()
