#!/bin/bash
#
# $Id: rudics.sh,v d1b0719fc3b6 2009/02/23 05:59:20 mikek $
#
# Start/stop the RUDICS server.
#
PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

LOGFILE=/var/log/rudics.log
PIDFILE=/var/run/rudics.pid
RUNDIR=/tmp

: ${RUDICS_PORT=10130}
: ${RUDICS_DBSERVER="http://50.18.188.61:5984"}
: ${RUDICS_DB="rudics"}
: ${RUDICS_CFG=/usr/local/etc/rudics.cfg}
: ${RUDICS_AUTH=/usr/local/etc/rudics.auth}

start ()
{
    > $LOGFILE

    [ -f $RUDICS_AUTH ] && auth="-a $RUDICS_AUTH"
    twistd -l $LOGFILE \
      -d $RUNDIR \
      --pidfile $PIDFILE \
      rudics -p $RUDICS_PORT -s $RUDICS_DBSERVER \
       -d $RUDICS_DB -c $RUDICS_CFG $auth
}

stop ()
{
    kill $(cat $PIDFILE 2> /dev/null) 2> /dev/null
}

status ()
{
    ps -p $(cat $PIDFILE 2>/dev/null) 1> /dev/null 2>&1 &&\
      echo "RUDICS server is running"
}

case "$1" in
        start)
                shift
                start
                ;;
        stop)
                stop
                ;;
        restart)
                stop
                start
                ;;
        status)
                status
                ;;
        *)
                echo "Usage: $0 {start|stop|restart}"
                ;;
esac
