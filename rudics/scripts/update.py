#!/usr/bin/env python
#
# Add new entries to the RUDICS database.
#
from optparse import OptionParser
from couchdb import client
import sys


def main():
    """
    %prog [options] [infile]

    Each line of input file must contain:

     email,IRSN,ipaddr:port,device name
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(server="http://localhost:5984", dbname="rudics")
    parser.add_option("-s", "--server",
                      type="string",
                      dest="server",
                      metavar="URL",
                      help="URL of CouchDB server (%default)")
    parser.add_option("-d", "--database",
                      type="string",
                      dest="dbname",
                      metavar="NAME",
                      help="name of CouchDB database (%default)")
    opts, args = parser.parse_args()
    if len(args) < 1:
        infile = sys.stdin
    else:
        infile = open(args[0], 'r')

    server = client.Server(opts.server)
    db = server[opts.dbname]

    for line in infile:
        email, irsn, forward, devname = line.strip().split(',')
        doc = {'imei': irsn, 'name': devname}
        if not forward:
            doc['type'] = 'local'
        else:
            doc['type'] = 'proxy'
            doc['owner'] = email
            doc['endpoint'] = forward

        old_doc = db.get(irsn)
        if old_doc:
            print "Updating entry: %s" % old_doc.id
            db[old_doc.id] = doc
        else:
            db[irsn] = doc

if __name__ == '__main__':
    main()
