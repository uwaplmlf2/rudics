#!/usr/bin/env python
#
# $Id: protocol.py,v 2d8149431e31 2008/07/07 21:08:37 mikek $
#
# Twisted-based protocol to redirect RUDICS log-in requests to another
# server.
#
# When a connection request arrives, the server sends the string
# 'irsn:' to the client. The client must respond with its Iridium Serial
# Number (IRSN) which must be pre-registered with the server. The server
# uses the IRSN to lookup the IP address and port to forward the connection
# to.
#
from twisted.protocols import basic, policies
from twisted.internet import defer
from twisted.python import log
from zope.interface import Interface
from twisted.cred import credentials, error
from twisted.internet.interfaces import ITransport
import base64
import socket
import struct
import time
from twisted.web.client import getPage
from urllib import quote_plus


class StatsMixin:
    """
    Mixin for protocols which want to count bytes in and out
    for a connection.
    """
    bytes_in = 0
    bytes_out = 0
    events = []

    def initCounters(self):
        self.bytes_in = 0
        self.bytes_out = 0
        self.events = []

    def registerInput(self, n):
        self.bytes_in += n

    def registerOutput(self, n):
        self.bytes_out += n

    def addEvent(self, name, t):
        self.events.append((name, t))

    def dumpStats(self):
        return {'transfer': (self.bytes_in, self.bytes_out),
                'events': self.events}


class Gatekeeper(basic.LineReceiver, policies.TimeoutMixin, StatsMixin):
    """
    Class to manage the connection request.
    """
    prompt = 'irsn: '
    delimiter = '\n'

    def __init__(self, portal, record_session=None, login_timeout=20):
        self.portal = portal
        self.record_session = record_session
        self.peer = None
        self.irsn = None
        self.initCounters()
        self.login_timeout = login_timeout
        self.setLineMode()
        self.setTimeout(None)

    def connectionMade(self):
        self.addEvent('connect', time.time())
        self.transport.write(self.prompt)
        self.setTimeout(self.login_timeout)

    def connectionLost(self, reason):
        self.addEvent('logout', time.time())
        if self.record_session:
            self.record_session(self.irsn, self.dumpStats())
        if self.peer:
            self.peer_close()

    def timeoutConnection(self):
        log.msg('Timeout waiting for IRSN')
        self.transport.loseConnection()

    def lineReceived(self, line):
        self.resetTimeout()
        irsn = line.strip(' \r\n\x00')
        if irsn:
            d = defer.maybeDeferred(self.authenticateIrsn, irsn)
            d.addCallback(self._cblogin, irsn)
            d.addErrback(self._cberror)
        else:
            self.transport.write(self.prompt)

    def rawDataReceived(self, data):
        if self.peer:
            self.registerInput(len(data))
            self.peer.write(data)

    def write(self, data):
        self.registerOutput(len(data))
        self.transport.write(data)

    def loseConnection(self):
        self.transport.loseConnection()

    def authenticateIrsn(self, irsn):
        """authenticate the supplied Irdium Serial Number"""
        if self.portal is not None:
            return self.portal.login(
                credentials.UsernamePassword(irsn, None),
                self,
                ITransport)
        raise error.UnauthorizedLogin()

    def _cblogin(self, ial, irsn):
        """ """
        interface, avatar, logout = ial
        if interface is not ITransport:
            log.err('_cblogin called with bad interface')
            return

        self.setTimeout(None)
        self.setRawMode()
        self.irsn = irsn
        self.peer = avatar
        self.peer_close = logout
        self.addEvent('login', time.time())

    def _cberror(self, error):
        self.setTimeout(None)
        self.transport.loseConnection()


class _addrList(object):
    """
    Efficient list of IP addresses.

    >>> L = _addrList(['10.1.15.0/24', '10.1.11.42'])
    >>> '10.1.15.20' in L
    True

    """
    def __init__(self, addresses):
        self.data = []
        for addr in addresses:
            try:
                base, bits = addr.split('/')
                bits = int(bits)
            except ValueError:
                base = addr
                bits = 32
            try:
                network = struct.unpack('>L', socket.inet_aton(base))[0]
                host_part = (1L << (32 - bits)) - 1L
                self.data.append((network, ~host_part))
            except (socket.error, struct.error):
                log.err('Bad address specification: ' + repr(addr))

    def __contains__(self, host):
        x = struct.unpack('>L', socket.inet_aton(host))[0]
        for network, mask in self.data:
            if (x & mask) == network:
                return True
        return False

    def __str__(self):
        return str(self.data)


class AllowDenyPeer(policies.WrappingFactory):
    """
    Allow or deny connections based on IP address.

    If the peer's address is in C{allow_list} then the connection is
    permitted, if the peer is in C{deny_list} the connection is not
    permitted, otherwise the connection is allowed.

    @ivar allow_list: list of allowed IP addresses (or address/netsize)
    @ivar deny_list: list of denied IP addresses (or address/netsize)
    """
    def __init__(self, wrappedFactory, allow_from=[], deny_from=[]):
        policies.WrappingFactory.__init__(self, wrappedFactory)
        self.allow_list = _addrList(allow_from)
        self.deny_list = _addrList(deny_from)

    def allow(self, addr):
        if addr in self.allow_list:
            return True
        elif addr in self.deny_list:
            return False
        else:
            return True

    def buildProtocol(self, addr):
        if self.allow(addr.host):
            return policies.WrappingFactory.buildProtocol(self, addr)
        log.msg('Connection refused from %s' % str(addr))
        return None


def encode_auth(user, pword):
    s = base64.encodestring('%s:%s' % (user, pword))
    return s.rstrip()


def utf8(x):
    """Convert object to unicode and encode as UTF-8."""
    return unicode(x).encode('utf-8')


def encode_param(name, value):
    """Return a URL-encoded parameter setting."""
    return '%s=%s' % (quote_plus(utf8(name)), quote_plus(utf8(value)))


def uploadForm(url, params, auth=()):
    query = [encode_param(name, value) for name, value in params]
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    if auth:
        headers['authorization'] = 'Basic %s' % encode_auth(*auth)
    return getPage(url, method='POST', headers=headers,
                   postdata='&'.join(query))
