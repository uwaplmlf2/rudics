#
# $Id: auth.py,v d1b0719fc3b6 2009/02/23 05:59:20 mikek $
#
from twisted.protocols import basic, policies
from twisted.internet import defer, protocol, reactor
from twisted.cred.portal import IRealm
from twisted.cred import credentials, checkers, error
from zope.interface import implements
from twisted.internet.interfaces import ITransport
from twisted.web import client
from twisted.python import log
import os
import pwd
from store import Device, MLF2Device, IMLF2Extras
try:
    from xml.etree import ElementTree as ET
except ImportError:
    from elementtree import ElementTree as ET
try:
    import json
except ImportError:
    import simplejson as json


def getfloatid(contents):
    xml = ET.fromstring(contents)
    float_id = xml.findtext('.//id')
    if float_id is not None:
        return int(float_id)
    else:
        log.msg('Bad float ID')
        raise ValueError


def mlf2IrsnCheck(irsn,
                  baseurl='http://mlf2srvr.apl.washington.edu/float/irsn/'):
    d = client.getPage(baseurl + irsn + '/', timeout=5)
    d.addCallback(getfloatid)
    return d


class Pipe(protocol.ProcessProtocol, policies.TimeoutMixin):
    """
    Protocol to manage the pipe between stdio and the login
    process.
    """
    def __init__(self, peer, timeout=60):
        self.peer = peer
        self.setTimeout(timeout)

    def outReceived(self, data):
        self.resetTimeout()
        self.peer.write(data)

    def finish(self):
        try:
            self.setTimeout(None)
            self.transport.loseConnection()
            self.transport.signalProcess('KILL')
        except:
            pass
    loseConnection = finish

    def write(self, data):
        self.transport.write(data)

    def timeoutConnection(self):
        log.msg('Session timeout')
        self.finish()
        self.peer.loseConnection()

    def errReceived(self, text):
        self.peer.write(text)

    def processEnded(self, reason):
        self.setTimeout(None)
        self.peer.loseConnection()


class Proxy (basic.LineReceiver):
    delimiter = 'irsn: '

    def __init__(self, peer):
        self.peer = peer
        self.setRawMode()

    def rawDataReceived(self, data):
        self.peer.write(data)

    def connectionLost(self, reason):
        self.peer.loseConnection()


class IrsnRealm (object):
    implements(IRealm)

    def __init__(self, timeout=60):
        self.connection_timeout = timeout

    def requestAvatar(self, avatarId, peer, *interfaces):
        log.msg('avatar Id: %s' % avatarId)
        method, irsn, rest = avatarId.split('$')
        if method == 'tcp':
            host, port = rest.split(':')
            c = protocol.ClientCreator(reactor, Proxy, peer)
            return c.connectTCP(host, int(port)).addCallback(self._cbconnect)
        elif method == 'shell':
            user, float_id = rest.split(':')
            info = pwd.getpwnam(user)
            sh_name = os.path.basename(info.pw_shell)
            env = {}
            env['IRSN'] = irsn
            env['FLOATID'] = float_id
            env['HOME'] = info.pw_dir
            env['USER'] = user
            env['LOGNAME'] = user
            env['PATH'] = '/usr/bin:/bin:/usr/local/bin'
            env['TERM'] = 'vt100'
            env['SHELL'] = sh_name
            if os.getuid() == 0:
                kwds = {'uid': info.pw_uid, 'gid': info.pw_gid}
            else:
                kwds = {}
            pipe = Pipe(peer, timeout=self.connection_timeout)
            reactor.spawnProcess(pipe,
                                 info.pw_shell, args=['-'+sh_name],
                                 env=env, usePTY=1,
                                 path=info.pw_dir, **kwds)
            return defer.succeed((ITransport, pipe, pipe.loseConnection))
        else:
            raise NotImplementedError('bad IRSN')

    def _cbconnect(self, proto):
        avatar = ITransport(proto)
        return ITransport, avatar, avatar.loseConnection


class IrsnDatabase (object):
    """
    Deprecated.

    Class to check an IRSN (Iridium Serial Number) and provide the RUDICS
    connection information to the Realm in the form of the avatarID. The
    format of the ID is:

        I{method}$I{irsn}$I{token}

    I{Method} is either C{tcp} or C{shell}. The C{tcp} method results in a
    connection to another in which case the I{token} is C{hostname}:C{port}.
    The C{shell} method is only for the MLF2 floats and results in a shell
    session on the local system (I{token} is C{username}:C{floatID}).

    The IRSN is first checked against the MLF2 database and if that fails it
    is checked against the local database.
    """
    implements(checkers.ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword,)

    def __init__(self, store, username='mlf2'):
        self.username = username
        self.store = store

    def getEntryStr(self, irsn):
        dev = self.store.findFirst(Device, Device.irsn==unicode(irsn))
        return str(dev.forward)

    def _addMlf2Entry(self, float_id, irsn):
        url = 'http://mlf2srvr.apl.washington.edu/float/%d/' % float_id
        dev = self.store.findOrCreate(Device, irsn=unicode(irsn))
        mlf = self.store.findOrCreate(MLF2Device,
                                      floatid=float_id, url=unicode(url))
        mlf.installOn(dev)
        return float_id

    def requestAvatarId(self, creds):
        """ """
        irsn = creds.username
        dev = self.store.findFirst(Device, Device.irsn == unicode(irsn))
        try:
            mlf = IMLF2Extras(dev)
            d = defer.succeed('shell$%s$%s:%d' % (irsn,
                                                  self.username,
                                                  mlf.floatid))
        except TypeError:
            d = mlf2IrsnCheck(irsn)
            d.addCallback(self._addMlf2Entry, irsn)
            d.addCallback(lambda float_id: 'shell$%s$%s:%d' % (irsn,
                                                               self.username,
                                                               float_id))
            d.addErrback(lambda err: 'tcp$%s$%s' % (irsn,
                                                    self.getEntryStr(irsn)))
            d.addErrback(lambda _: error.UnauthorizedLogin())
        return d


class IrsnCouchdb (object):
    """
    Class to check an IRSN (Iridium Serial Number) and provide the RUDICS
    connection information to the Realm in the form of the avatarID. The
    format of the ID is:

        I{method}$I{irsn}$I{token}

    I{Method} is either C{tcp} or C{shell}. The C{tcp} method results in a
    connection to another in which case the I{token} is C{hostname}:C{port}.
    The C{shell} method is only for the MLF2 floats and results in a shell
    session on the local system (I{token} is C{username}:C{floatID}).
    """
    implements(checkers.ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword,)

    def __init__(self, server, dbname='rudics', username='mlf2'):
        self.username = username
        self.server = server
        self.dbname = dbname
        self.cache = dict()

    def requestAvatarId(self, creds):
        """ """
        def _createAvatarId(doc):
            self.cache[doc['imei']] = doc
            if doc['type'] == 'local':
                _, float_id = doc['name'].split('-')
                return 'shell$%s$%s:%s' % (doc['imei'],
                                           self.username,
                                           float_id)
            else:
                return 'tcp$%s$%s' % (doc['imei'], doc['endpoint'])

        def _check_cache(err):
            return self.cache.get(creds.username, error.UnauthorizedLogin())

        irsn = creds.username
        d = client.getPage(self.server + '/' + self.dbname + '/' + irsn,
                           timeout=5)
        d.addCallback(lambda contents: json.loads(contents))
        d.addErrback(_check_cache)
        d.addCallback(_createAvatarId)
        return d
