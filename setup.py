#!/usr/bin/env python
#
# $Id: setup.py,v d1b0719fc3b6 2009/02/23 05:59:20 mikek $
#
from setuptools import setup

VERSION = '2.3'

setup(
    name='RudicsServer',
    version=VERSION,
    description='Server to handle Iridium RUDICS login requests',
    author='Michael Kenney',
    packages=['rudics',  'rudics.scripts', 'twisted.plugins'],
    package_data={'twisted' : ['plugins/*_plugin.py'],
                  'rudics.scripts' : ['*.sh']},
    install_requires=['Twisted>=2.5'],
    include_package_data=True,
    zip_safe=False,
    entry_points={
    'console_scripts' : [
            'rudics-install = rudics.scripts.install:install_startup',
            'rudics-update = rudics.scripts.update:main',
            'rudics-delete = rudics.scripts.delete:main',
            'rudics-dump = rudics.scripts.dump:main']})
